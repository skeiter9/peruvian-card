<?php
session_start();
header ("Expires: Sat, 12 Jul 2008 09:00:00 GMT");
header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); 
header ("Cache-Control: no-store, no-cache, must-revalidate");
header ("Cache-Control: post-check=0, pre-check=0",false);
header ("Pragma: no-cache");

date_default_timezone_set('America/Lima');
$clave = md5(microtime() * mktime());
$texCaptcha = substr($clave,0,5);
$captcha = imagecreatefromjpeg("images/bg-captcha.jpg");
$black = imagecolorallocate($captcha, 0, 0, 0);			//colores RGB
$line = imagecolorallocate($captcha,233,239,239);		//colores RGB
imageline($captcha,0,0,39,29,$line);
imageline($captcha,40,0,64,29,$line);
$cad="";
for($i=0;$i<strlen($texCaptcha);$i++)
	$cad.= $texCaptcha[$i]." ";
imagestring($captcha, 20, 15, 5, $cad, $black);			/*	escribimos aleatoriamente en la imagen*/
$_SESSION['captcha'] = md5($texCaptcha);
header('Content-Type: image/png');
imagepng($captcha);
imagedestroy($captcha);
?> 